

function problem6(inventory = []){

    if (Array.isArray(inventory) && inventory.length >= 1){
        let arr = [];
        for(let i = 0; i < 50; i++){

            let x = inventory[i].car_make;

            if (x == "BMW" || x == "Audi"){

                arr.push(inventory[i]);
            
            }
        }    
        return arr;
    }
    else{
        return [];
    }
    
    
}



module.exports = problem6;