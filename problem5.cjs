

function problem5(inventory = []){

    if (Array.isArray(inventory) && inventory.length >= 1){

        let arr = [];
        for(let i = 0; i < 50; i++){

            let x = inventory[i].car_year;

            if (x < 2000){

                arr.push(x);
            
            }
        }    
        return arr;
    }
    else{
        return [];
    }
    
}





module.exports = problem5;