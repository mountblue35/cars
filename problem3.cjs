

function problem3(inventory=[]){
    if (Array.isArray(inventory) && inventory.length >= 1){
        
        let arr = [];
        for(let i = 0; i < 50; i++){

            let x = inventory[i].car_model;
            arr.push(x);
        }    
        arr.sort();
        return arr;
    }
    else{
        return []
    }
    
}


module.exports = problem3;
