

function problem2(inventory=[]){

    if(Array.isArray(inventory) && inventory.length >= 1){
        // console.log("here")
        let len = inventory.length;

        let object_found = inventory[len-1];
        // console.log(object_found);
        return object_found;

    }else{
        return []
    }    
    
}

module.exports = problem2;
